import sys
import compileall
import pathlib as plib
import shutil
import os

os.chdir(plib.Path(__file__).parent)

vinfo = sys.version_info
py_num = f'{vinfo.major}{vinfo.minor}'


m_dist_app_path = plib.Path(f'../dist/app{vinfo.major}{vinfo.minor}')
m_dist_src_path = m_dist_app_path / 'src'
m_dist_web_path = m_dist_app_path / 'web'
m_dist_main_path = m_dist_app_path / 'main.pyc'
m_dist_startup_path = m_dist_app_path / 'startup.bat'

compileall.compile_dir(r'../src')
compileall.compile_file('../main.py')
print('编译完毕')

if os.path.exists(m_dist_app_path):
    shutil.rmtree(m_dist_app_path)

os.makedirs(m_dist_app_path)

print('清除旧文件完毕')


def to_fileName(file: plib.Path):
    arr = file.name.split('.')
    return '.'.join([arr[0], arr[2]])


def to_folder(file: plib.Path):
    parts = list(file.parts)
    del parts[-2]
    parts[-1] = to_fileName(file)
    # parts = [plib.Path(p) for p in parts]

    parts = [p for p in parts if not all(s in ['.', '/'] for s in p)]

    parts = plib.Path.joinpath(m_dist_app_path, *parts)
    return parts


fs = plib.Path('../src').rglob(f'*.cpython-{py_num}.pyc')
fs = [(p, to_folder(p)) for p in fs]

for f in fs:
    if not os.path.exists(f[1].parent):
        os.makedirs(f[1].parent)

    shutil.copyfile(f[0], f[1])


shutil.copyfile(f'../__pycache__/main.cpython-{py_num}.pyc', m_dist_main_path)

shutil.copytree('../web', m_dist_web_path)


for f in plib.Path('others').glob('*'):
    shutil.copyfile(f, m_dist_app_path / f.name)
# shutil.copyfile('../startup.bat', m_dist_startup_path)

print(f'创建完成-({vinfo})')
