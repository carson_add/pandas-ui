
import unittest

from src.dataModel.pandasCmd import PandasCmd


class PandasCmd2modelTest(unittest.TestCase):

    def test_(self):
        import pandas as pd
        df = pd.DataFrame({
            'name': 'x1 x2 x3 x4'.split(),
            'n1': [1, 2, 3, 4],
            'n2': [1, 2, 3, 4],
        })

        cmd = PandasCmd.from_df(df)
