
import unittest

from src.core.Commander import Commander, MethodCall, Statement


class CommanderTest(unittest.TestCase):

    def test_query1(self):
        st = Statement('df')
        st.query('name=="x1"')

        expected = '''df.query('name=="x1"')'''
        act = st.to_code()

        self.assertEqual(expected, act)

    def test_query2(self):
        st = Statement('df')
        st.query("name=='x1'")

        expected = '''df.query("name=='x1'")'''
        act = st.to_code()

        self.assertEqual(expected, act)

# unittest.main()
