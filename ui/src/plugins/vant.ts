import { App } from "vue";

import {
    Button, Form, Tabs, Tag, Steps, AutoComplete, Table,
    Input, Popover, Select, Upload, Divider,
    Descriptions, Popconfirm, Spin, Modal, Result, Empty, InputNumber, Card, Affix
} from "ant-design-vue";

import { InboxOutlined, StarFilled } from '@ant-design/icons-vue';

import 'ant-design-vue/dist/antd.css';


export default {
    install: (app: App) => {
        [Button, Form, Tabs, Tag, Steps, AutoComplete,
            Input, Popover, Select, Upload, Divider,
            Descriptions, Popconfirm, Spin, Modal, Result, Empty, InputNumber, Affix, Table, Card].forEach(v => app.use(v));

        app.component('inboxOutlined', InboxOutlined)
    }
}