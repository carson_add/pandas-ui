import { CmdResult } from "@/models/PandasCmdModel";

import axios from "axios";
import * as DfFilters from "@/models/DfFilters";
import { FileArgsType, UploadFile } from '@/models/DataLoadModel';
import { GroupbyModel } from '@/models/GroupbyModel';
import { Command } from '@/models/CommanderModel';
import { TableHandle } from '@/models/TableHandleModel';




export async function uploadFile(file: UploadFile, args: FileArgsType) {
    const param = new FormData();
    param.append("file", file.file);

    // const param = {
    //     file: fileParam,
    //     ext: file.getFileExt(),
    //     args: JSON.stringify(args)
    // }

    param.append('file_name', file.file.name)
    param.append('ext', file.getFileExt())
    param.append('args', JSON.stringify(args))

    const config = {
        headers: { "Content-Type": "multipart/form-data" },
    };

    const ret = await axios.post('api/upload_file', param, config)
    return ret.data as CmdResult
    // return ret.data
}



export async function query(queryString: string) {
    const ret = await axios.post('api/query', { query: queryString })
    return ret.data as CmdResult
}


export async function getPyCode() {
    const ret = await axios.get('api/py_cody')
    return ret.data as string
}

export async function getAllCmds() {
    const ret = await axios.get('api/cmds')
    return ret.data as Command[]
}


export async function groupby(model: GroupbyModel) {
    const ret = await axios.post('api/groupby', model)
    return ret.data as CmdResult
}

export async function removeCmd(id: string) {
    const ret = await axios.post('/api/remove/cmd', { id })
    return ret.data as CmdResult
}

export async function uploadFilters(filterList: DfFilters.FilterList) {
    const ret = await axios.post('api/table/filters', filterList)
    return ret.data as CmdResult
}

export async function tableHandle(tableHandle: TableHandle) {
    const ret = await axios.post('api/table/handle', tableHandle)
    return ret.data as CmdResult
}
