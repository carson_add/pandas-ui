import { Store, StoreDataType } from "./Store";
import { computed, reactive, ref } from 'vue';
import { DataLoadModel, UploadFile, FileArgsType, ExcelArgs, CsvArgs, FeatherArgs } from '@/models/DataLoadModel';



class DataLoadStore extends Store<DataLoadModel> {

    private extMapping: { [key: string]: () => FileArgsType } = {
        'xlsx': this.getExcelDefaultArgs,
        'xls': this.getExcelDefaultArgs,
        'xlsm': this.getExcelDefaultArgs,
        'csv': this.getCsvDefaultArgs,
        'feather': this.getFeatherDefaultArgs,
    }


    protected data(): StoreDataType<DataLoadModel> {
        let File = ref(null as unknown as UploadFile)
        let Args = ref({} as FileArgsType)


        return reactive({
            File,
            Args
        })
    }


    /**
     * 
     */
    public setFile(file: File) {
        this.state.File = new UploadFile(file)

        if (this.state.File.isSupportExt) {
            this.state.Args = this.extMapping[this.state.File.getFileExt()]()
        }

    }



    private getExcelDefaultArgs(): ExcelArgs {
        return new ExcelArgs()
    }


    private getCsvDefaultArgs(): CsvArgs {
        return new CsvArgs()
    }

    private getFeatherDefaultArgs(): FeatherArgs {
        return new FeatherArgs()
    }

}

export default new DataLoadStore()