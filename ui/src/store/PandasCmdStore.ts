import { Store, StoreDataType } from "./Store";
import { computed, reactive, ref } from 'vue';
// import { PandasCmdResult, PandasCmdResultType } from '@/models/PandasCmdResult';

import { CmdResult, PandasCmdResultType } from "@/models/PandasCmdModel";
import { ColumnInfo, ColumnInfoItem, Table } from '@/models/DataFrameModel';

interface PandasCmdStoreModel {
    type: PandasCmdResultType,
    message: string,
    code: string
}

class PandasCmdStore extends Store<PandasCmdStoreModel> {
    protected data(): PandasCmdStoreModel {

        return reactive({
            type: PandasCmdResultType.None,
            message: '',
            code: ''
        })


    }


    public SetError(result: CmdResult) {
        this.state.type = result.type as PandasCmdResultType
        this.state.message = result.message
        this.state.code = result.code
    }

    public SetCorrect() {
        this.state.type = PandasCmdResultType.Success
        this.state.message = ''
        this.state.code = ''
    }

    public Clear() {
        this.state.type = PandasCmdResultType.None
    }


}

export default new PandasCmdStore()