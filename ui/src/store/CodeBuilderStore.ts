import { Store, StoreDataType } from "./Store";
import { computed, reactive, ref } from 'vue';
import { CodeBuilderModel } from '@/models/CodeBuilderModel';

class CodeBuilderStore extends Store<CodeBuilderModel> {




    protected data(): StoreDataType<CodeBuilderModel> {
        let code = ref('')


        return reactive({
            code
        })
    }


    /**
     * update
code:string     */
    public update(code: string) {
        this.state.code = code
    }

    /**
     * Clear
     */
    public Clear() {
        this.state.code = ''
    }



}

export default new CodeBuilderStore()