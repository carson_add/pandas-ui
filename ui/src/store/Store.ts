import { reactive, readonly, ToRefs, toRefs, Ref, UnwrapRef } from "vue";

export type StoreDataType<T> = T extends Ref ? T : UnwrapRef<T>;



export abstract class Store<T extends Object> {
    protected state: T

    constructor() {
        this.state = this.data() as T
    }

    protected abstract data(): StoreDataType<T>

    public getStateReadonly(): T {
        return readonly(this.state) as T
    }

    public getStateToRefs(): ToRefs<T> {
        return toRefs(this.state)
    }


}

