import { Store, StoreDataType } from "./Store";
import { computed, reactive, ref } from 'vue';
import { Command } from '@/models/CommanderModel';





export interface CommandStoreModel {
    cmds: Command[]
}


class DataframeStore extends Store<CommandStoreModel> {
    protected data(): StoreDataType<CommandStoreModel> {
        let cmds = [] as Command[]
        return reactive({
            cmds
        })
    }



    setData(cmds: Command[]): void {
        this.state.cmds = cmds
    }


    public appendCmd(cmd: Command) {
        this.state.cmds.push(cmd)
    }

}

export default new DataframeStore()