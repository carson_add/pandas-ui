import { computed, reactive, ref } from 'vue';
import { Store, StoreDataType } from "./Store";

interface Test {
    count: number,
    rows: number[]
}


class TestStore extends Store<Test> {
    protected data(): StoreDataType<Test> {

       return {
           count:1,
           rows:[1]
       }
    }



}


export default new TestStore()