import { Store, StoreDataType } from "./Store";

import { ColumnInfo, ColumnInfoItem, DataFrame, Field } from "@/models/DataFrameModel";

import { computed, reactive, ref, toRaw } from 'vue';


import * as apps from "@/applications";
import { SortOrderType, TableHandle } from "@/models/TableHandleModel";

export interface DataframeStoreModel {
    data: object[]
    columns: object[]
    columnInfo: ColumnInfoItem[]
    isLoaded: boolean
}


class DataframeStore extends Store<DataframeStoreModel> {
    protected data(): StoreDataType<DataframeStoreModel> {
        return reactive({
            data: [],
            columns: [],
            columnInfo: {} as ColumnInfoItem[],
            isLoaded: computed(() => { return this.state.columns.length > 0 })
        })
    }




    setData(fields: Field[] | null, data: object[] | null, columnInfo: ColumnInfoItem[] | null): void {

        if (fields !== null) {
            this.state.columns = fields
        }

        if (data !== null) {
            this.state.data = data

        }
        if (columnInfo !== null) {
            this.state.columnInfo = columnInfo
        }



    }


    /**
     * getData
     */
    public getData() {
        return this.getStateToRefs().data
    }

    public getColumns() {
        return this.getStateToRefs().columns
    }

    public getstate() {
        return this.state
    }



}

export default new DataframeStore()