import { reactive, Ref, ref } from "vue";
import * as apps from "@/applications";

import DataLoadStore from "@/store/DataLoadStore";



function useFileload() {
    let fileArgsModalShowed = ref(false)
    let loading = ref(false)

    function beforeUpload(file: File) {
        DataLoadStore.setFile(file)
        fileArgsModalShowed.value = true
        return false
    }


    async function onOk() {
        loading.value = true
        await apps.loadDataFromFile()
        loading.value = false
        fileArgsModalShowed.value = false
    }



    return {
        beforeUpload,
        fileArgsModalShowed, onOk, loading
    }
}




export default useFileload