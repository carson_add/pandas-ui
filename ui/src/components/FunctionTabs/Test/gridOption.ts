import { reactive, toRaw } from "vue";
import axios from "axios";

export function useGridOptions() {

    const opt = reactive({



        sortConfig: {
            trigger: 'cell',
            remote: true, multiple: true
        },
        filterConfig: {
            remote: true
        },
        proxyConfig: {
            autoLoad: true,
            filter: true,
            sort: true,
            ajax: {
                // 任何支持 Promise API 的库都可以对接（fetch、jquery、axios、xe-ajax）
                query: ({ filters, sorts }: any) => {
                    sorts = toRaw(sorts)
                    console.log("sort", JSON.stringify(sorts));
                    console.log("filters", JSON.stringify(filters));
                    if (filters.length !== 0) {
                        filters = toRaw(filters);
                        const colname = filters[0].property;
                        const filter_values = filters[0].values;
                        console.log(colname, filter_values);
                    }

                    return axios.get("/api/user/list").then(v => v.data)

                }
            }
        },
        columns: [
            { type: "checkbox", width: 50 },
            { type: "seq", width: 60 },
            {
                field: "name",
                title: "Name",
                filters: [
                    { value: "a1", label: "a1" },
                    { value: "a2", label: "a2" },
                ], sortable: true,
            },
            {
                field: "nickname", title: "Nickname", filters: [
                    { value: "a1", label: "a1" },
                    { value: "a2", label: "a2" },
                ], sortable: true
            },
            { field: "role", title: "Role", sortable: true },
        ]
    })


    return opt
}


