import Mock from 'mockjs';
import table from "./table.json";
import queryTable from "./query_table.json";

import groupbyTable from "./groupby.json";

import colsInfos from "./cols_infos.json";
import pandasError from "./pandas_errors.json";

import DfStore from "../store/DfStore";

import cmds from "./commandersModel.json";


import file_upload from '@/mock/file_upload.json';

import code from '@/mock/code.json';

import test_json from "./test.json";

if (process.env.NODE_ENV == "development") {
    console.log("正在模拟数据中");
    Mock.mock("api/upload_file", file_upload);

    Mock.mock(RegExp('api/query' + '.*'), queryTable);

    Mock.mock(RegExp('api/groupby' + '.*'), groupbyTable);

    Mock.mock(RegExp('api/columns_infos' + '.*'), colsInfos);

    // Mock.mock(RegExp('api/query' + '.*'), pandasError);


    Mock.mock(RegExp('api/table/filters' + '.*'), queryTable);


    Mock.mock(RegExp('api/py_cody' + '.*'), 'import tests');
    Mock.mock(RegExp('api/cmds' + '.*'), cmds);

    Mock.mock(RegExp('api/table/handle' + '.*'), test_json);
}

import Tss from "../store/TestStroe";
import CodeBuilderStore from '@/store/CodeBuilderStore';
import CommandStore from '@/store/CommandStore';

import * as apps from "@/applications";
import axios from 'axios';
import PandasCmdStore from '@/store/PandasCmdStore';

export default function autoLoadData() {
    if (process.env.NODE_ENV == "development") {
        console.log("自动填充数据");



        DfStore.setData(table.table.fields, table.table.data, table.columnInfos)
        PandasCmdStore.SetCorrect()

        CodeBuilderStore.update(code.code)

        CommandStore.setData(cmds)

        apps.updateAllCommands()
    }
}