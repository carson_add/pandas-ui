




export class UploadFile {
    private _file: File

    public _excelExts = ['xlsx', 'xls', 'xlsm']
    public _csvExts = ['csv']
    public _featherExts = ['feather']
    private _allExts = this._excelExts.concat(this._csvExts).concat(this._featherExts)

    constructor(file: File) {
        this._file = file
    }

    public get file(): File {
        return this._file
    }

    public getFileExt(): string {
        let arr = this.file.name.split('.')
        return arr[arr.length - 1]
    }


    public get allSupportExts(): Array<string> {
        return this._allExts
    }



    public get isExcel(): boolean {
        let exts = this._excelExts
        return exts.indexOf(this.getFileExt()) > -1
    }


    public get isCsv(): boolean {
        return this.getFileExt() === this._csvExts[0]
    }

    public get isFeather(): boolean {
        return this.getFileExt() === this._featherExts[0]
    }


    public get isSupportExt(): boolean {
        return this._allExts.indexOf(this.getFileExt()) > -1
    }


}


export class ExcelArgs {
    header: number | number[]
    sheet_name: number

    constructor(sheetName: number = 0, header: number | number[] = 0) {
        this.sheet_name = sheetName
        this.header = header
    }
}

export class CsvArgs {
    encoding: string

    constructor(encoding: string = 'utf8') {
        this.encoding = encoding
    }
}

export class FeatherArgs {

}



export type FileArgsType = ExcelArgs | CsvArgs | FeatherArgs


export interface DataLoadModel {
    File: UploadFile
    Args: FileArgsType
}