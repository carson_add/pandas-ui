

export type filterValueType = string | number | Date

export enum SortOrderType {
    Asc = 'asc',
    Desc = "desc"
}





export interface Filter {
    field: string
    values: filterValueType[]
}

export interface FilterList {
    filters: Filter[]
}

export interface Sort {
    field: string
    order: SortOrderType
}

export interface SortList {
    sorts: Sort[]
}


export class TableHandle {
    private filters: Filter[] = []
    private sort: Sort = <Sort>{}

    /**
     * addFilters
filters:     */
    public addFilters(filters: { dataIndex: string, values: filterValueType[] }[]) {
        let fs: Filter[] = filters.map(v => {
            return {
                field: v.dataIndex,
                values: v.values
            }
        })

        this.filters = fs
    }

    public addSort(sort: { columnKey: string, order: SortOrderType }) {
        let ss = {
            field: sort.columnKey,
            order: sort.order
        }

        this.sort = ss
    }


}