
export interface Table {
    fields: Field[];
    data: object[];
}



export interface Field {
    dataIndex: string
    title: string;
    filters: Filter[];
}

export interface Filter {
    text: string;
    value: string | number;
}




export interface ColumnInfoItem {
    field: string;
    label: string;
    type: string;
    ui_type: string
}


export interface ColumnInfo {
    columnInfos: ColumnInfoItem[]
}


export interface DataFrame {
    table: Table
    columnInfo: ColumnInfo
    isLoaded?: boolean
}


