


export interface Command {
    _id: string;
    name: string;
    code: string;
    hasError: boolean;
}

