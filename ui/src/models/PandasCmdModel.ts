import { Table } from './DataFrameModel';
import { ComputedRef } from 'vue';
import { ColumnInfoItem } from './DataFrameModel';


export enum PandasCmdResultType {
    None = 'none',
    Error = "error",
    Success = "success"
}

export interface CmdResult {
    type: PandasCmdResultType | string;
    columnInfos: ColumnInfoItem[];
    table: Table;
    code: string;
    message: string;
}
