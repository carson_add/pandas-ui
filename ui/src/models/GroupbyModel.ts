

export interface GroupbyAggModel {
    filed: string
    methods: string[]
}


export class GroupbyModel {
    private _keys: string[] = []
    private _aggs: GroupbyAggModel[] = []

    constructor() {


    }


    /**
     * setKeys
keys:string[]     */
    public setKeys(keys: string[]) {
        this._keys = keys
    }

    /**
     * addAggs
     */
    public addAggs(filed: string, methods: string[]) {
        this._aggs.push({ filed, methods })
    }

}