let opts = [{
    libraryName: "ant-design-vue",
    style: true,
},
{
    libraryName: "vxe-table",
    style: true,
}
];

// ["@vue/babel-plugin-jsx"]

module.exports = {
    presets: ["@vue/cli-plugin-babel/preset"],
    plugins: ["@vue/babel-plugin-jsx",
        [
            "import", {
                libraryName: "ant-design-vue", style: false,
            },
            'ant-design-vue'
        ],
        // [
        //     "import", {
        //         libraryName: "vxe-table", style: false,
        //     },
        //     'vxe-table'
        // ],
        [
            "import", {
                libraryName: "ant-design/icons-vue", "libraryDirectory": 'lib/icons', style: false,
            },
            'ant-design-icons'
        ]
    ],
};
